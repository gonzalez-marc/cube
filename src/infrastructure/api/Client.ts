import axios, { Axios, type AxiosRequestHeaders, type AxiosResponse } from 'axios';

class Client {
	private axios!: Axios;

	public constructor() {
		this.setupAxios();
	}

	private setupAxios() {
		this.axios = axios.create({
			baseURL: 'https://api.thecatapi.com/v1/images/'
		});

		this.axios.interceptors.request.use(
			(config) => {
				config.headers = {
					Accept: 'application/json',
					'Content-Type': 'application/json;charset=UTF-8'
				};

				// TODO implémenter la méthode authenticate
				// config.headers.Authorization = 'Bearer ' + this.authenticate()

				return config;
			},
			(error) => {
				Promise.reject(error);
			}
		);

		// TODO implémenter la méthode authenticate
		//this.axios.interceptors.response.use(
		//	(response) => response,
		//	async (error) => {
		//    const originalRequest = error.config

		//    if(error.response.status === 401) {
		//      const accessToken = this.authenticate()
		//      originalRequest.headers.Authorization = 'Bearer ' + accessToken

		//      return this.axios.request(originalRequest)
		//	}
		//);
	}

	public async get<T>(url: string): Promise<AxiosResponse<T>> {
		return this.axios.get(url);
	}

	public async post<T>(url: string, payload: T): Promise<AxiosResponse> {
		return this.axios.post(url, payload);
	}

	public async put<T>(url: string, payload: T): Promise<AxiosResponse> {
		return this.axios.put(url, payload);
	}

	public async delete(url: string): Promise<AxiosResponse> {
		return this.axios.delete(url);
	}
}

export default new Client();
